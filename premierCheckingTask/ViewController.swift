//
//  ViewController.swift
//  premierCheckingTask
//
//  Created by Sierra 4 on 02/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.

import UIKit
import QuartzCore

class ViewController: UIViewController {
    
    @IBOutlet weak var lowerLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var UpperLabel: UILabel!
    
    var banks:[Bank]?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        banks = Bank.all()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func rotateView()
    {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.collectionView.transform = self.collectionView.transform.rotated(by: CGFloat(M_PI_4))
        })
    }
}

extension ViewController:UICollectionViewDataSource,UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return (banks?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let CollctionViewcell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! CircularCollectionViewCell
        CollctionViewcell.imageName = (banks?[indexPath.row].img!)!
        return CollctionViewcell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        UpperLabel.text = banks?[indexPath.item].uprLabel
        lowerLabel.text = banks?[indexPath.item].lwrLabel
    }
}

