//
//  Bank.swift
//  premierCheckingTask
//
//  Created by Sierra 4 on 06/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation

class Bank {
    
    var uprLabel:String?
    var lwrLabel:String?
    var img:String?
    
    init(uprLabel: String, lwrLabel: String, img: String)
    {
        self.uprLabel = uprLabel
        self.lwrLabel = lwrLabel
        self.img = img
    }
    static func all() -> [Bank]
    {
        let bank1 = Bank(uprLabel: "Plaid - Premier-Checking", lwrLabel: "Checking - 5204", img: "71d4ee179196fd6c889f86103627785e")
        let bank2 = Bank(uprLabel: "Plaid - Premier ", lwrLabel: "Checking - 5204", img: "hdfc-bank.jpeg")
        let bank3 = Bank(uprLabel: "checking - 5000", lwrLabel: "Checking - 5204", img: "ing-insurance-new-premiums-ingeasi-for-family-a")
        let bank4 = Bank(uprLabel: "checking - 50401", lwrLabel: "Checking - 5204", img: "logo-NFA.jpeg")
        let bank5 = Bank(uprLabel: "Plaid Premier Checking",lwrLabel: "Checking - 5204", img: "images-5.jpeg")
        let bank6 = Bank(uprLabel: "Checking - 52043",lwrLabel: "Checking - 5204", img: "images-6.jpeg")
        let bank7 = Bank(uprLabel: "checking - 50003", lwrLabel: "Checking - 5204", img: "images-7.jpeg")
        let bank8 = Bank(uprLabel: "checking - 50400",  lwrLabel: "Checking - 5204", img: "images-8.jpeg")
        let bank9 = Bank(uprLabel: "checking -50220",  lwrLabel: "Checking - 5204",  img: "images-9.jpeg")
        let bank10 = Bank(uprLabel: "checking -507200", lwrLabel: "Checking - 5204", img: "images-10.jpeg")
        let bank11 = Bank(uprLabel: "checking -50700", lwrLabel: "Checking - 5904", img: "State-Bank-Of-India1")
        let bank12 = Bank(uprLabel: "checking -504500", lwrLabel: "Checking - 5204", img: "Orix.jpeg")
        
        // let images: [String] = Bundle.main.paths(forResourcesOfType: "jpeg", inDirectory: "images")
        
        return [bank1, bank2, bank3, bank4, bank5, bank6, bank7, bank8, bank9, bank10,bank11, bank12]
    }
}
