//
//  circleArc.swift
//  premierCheckingTask
//
//  Created by Sierra 4 on 03/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit

let NoOfGlasses = 8
let π:CGFloat = CGFloat(M_PI)

@IBDesignable class CounterView: UIView
{
    @IBInspectable var counter: Int = 5
    @IBInspectable var counterColor: UIColor = UIColor.gray
    
    override func draw(_ rect: CGRect)
    {
        let center = CGPoint(x:145, y: 180)
        let radius: CGFloat = 300
        let arcWidth: CGFloat = 4
        let startAngle: CGFloat =  π / 2
        let endAngle: CGFloat = π / 3
        let path = UIBezierPath(arcCenter: center,radius: radius/2 - arcWidth/2,startAngle: startAngle,
                                endAngle: endAngle,clockwise: true)
        path.lineWidth = arcWidth
        counterColor.setStroke()
        path.stroke()
    }
}
